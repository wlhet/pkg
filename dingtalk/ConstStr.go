package dingtalk

const (
	//call_back
	regCallBackUrl    = "https://oapi.dingtalk.com/call_back/register_call_back?access_token=%s" //注册
	updateCallBackUrl = "https://oapi.dingtalk.com/call_back/update_call_back?access_token=%s"   //更新
	getCallBackUrl    = "https://oapi.dingtalk.com/call_back/get_call_back?access_token=%s"      //查询
	deleteCallBackUrl = "https://oapi.dingtalk.com/call_back/delete_call_back?access_token=%s"

	//access_token
	getTokenUrl = "https://oapi.dingtalk.com/gettoken?appkey=%s&appsecret=%s" //获取access_token

	//sendmsg
	sendMsgToUserUrl = "https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2?access_token=%s" //应用发送消息给用户

	//process
	processinstanceUrl          = "https://oapi.dingtalk.com/topapi/processinstance/get?access_token=%s"          //获取审批实例地址
	getProNameUrl               = "https://oapi.dingtalk.com/topapi/process/get_by_name?access_token=%s"          //获取审批Id
	processinstanceTerminateUrl = "https://oapi.dingtalk.com/topapi/process/instance/terminate?access_token=%s"   //终止实例接口地址
	processinstanceCommitAddUrl = "https://oapi.dingtalk.com/topapi/process/instance/comment/add?access_token=%s" //评论接口

	//user
	getUsersNumUrl       = "https://oapi.dingtalk.com/topapi/user/count?access_token=%s"          //获取企业用户数
	geIdByMobileUrl      = "https://oapi.dingtalk.com/topapi/v2/user/getbymobile?access_token=%s" //通过手机号获取用户ID
	getUserUrl           = "https://oapi.dingtalk.com/topapi/v2/user/get?access_token=%s"         //通过用户ID获取用户详细信息
	updateUserUrl        = "https://oapi.dingtalk.com/topapi/v2/user/update?access_token=%s"      //更新用户信息
	createUserUrl        = "https://oapi.dingtalk.com/topapi/v2/user/create?access_token=%s"      //创建用户
	getUserInfoByCodeUrl = "https://oapi.dingtalk.com/user/getuserinfo?access_token=%s&code=%s"   //钉钉免登码获取用户信息

	//Department
	departmentCreateUrl           = "https://oapi.dingtalk.com/topapi/v2/department/create?access_token=%s"           //创建部门
	departmentUpdateUrl           = "https://oapi.dingtalk.com/topapi/v2/department/update?access_token=%s"           //更新部门
	departmentDeleteUrl           = "https://oapi.dingtalk.com/topapi/v2/department/delete?access_token=%s"           //删除部门
	departmentGetUrl              = "https://oapi.dingtalk.com/topapi/v2/department/get?access_token=%s"              //部门详情
	departmentListsubidUrl        = "https://oapi.dingtalk.com/topapi/v2/department/listsubid?access_token=%s"        //获取子部门ID列表
	departmentListparentbyuserUrl = "https://oapi.dingtalk.com/topapi/v2/department/listparentbyuser?access_token=%s" //获取指定用户的所有父部门列表
	departmentListparentbydeptUrl = "https://oapi.dingtalk.com/topapi/v2/department/listparentbydept?access_token=%s" //获取指定部门的所有父部门列表
	departmentListsubUrl          = "https://oapi.dingtalk.com/topapi/v2/department/listsub?access_token=%s"          //获取部门列表
)
