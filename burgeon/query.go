package burgeon

func (this *BurgeonConnection) NewQuery() PostData {
	return PostData{Id: 1, Command: "Query", Params: make(map[string]interface{})}
}

//设置查询的表
func (this *PostData) QuerySetTable(t string) {
	this.Params["table"] = t
}

//设置返回结果
func (this *PostData) QuerySetResult(columns ...string) {
	this.Params["columns"] = columns
}

//设置查询调节  k 条件  v 值
func (this *PostData) QuerySetCondition(k string, v string) {
	params := make(map[string]interface{})
	params["column"] = k
	params["condition"] = "=" + v
	this.Params["params"] = params
}

func (this *PostData) QuerySetStartRange(start, rg int) {
	this.Params["start"] = start
	this.Params["range"] = rg
}

//设置查询调节  k 条件  v 值
func (this *PostData) QuerySetConditionExpr1(k, v string) {
	params := this.Params["params"].(map[string]interface{})
	expr1 := make(map[string]interface{})
	expr1["column"] = k
	expr1["condition"] = "=" + v
	params["expr1"] = expr1
}

//设置查询调节  k 条件  v 值
func (this *PostData) QuerySetConditionExpr2(k, v string) {
	params := this.Params["params"].(map[string]interface{})
	expr2 := make(map[string]interface{})
	expr2["column"] = k
	expr2["condition"] = "=" + v
	params["expr2"] = expr2
}

//设置查询调节  k 条件  v 值
func (this *PostData) QuerySetConditionCombine(v string) {
	params := make(map[string]interface{})
	params["combine"] = v
	this.Params["params"] = params
}
