package api

import (
	"errors"
	"fmt"
	"strconv"

	"gitee.com/wlhet/pkg/burgeon"
	"gitee.com/wlhet/pkg/burgeon/util"

	"gitee.com/wlhet/pkg/gjson"
)

//条码检测
//传入 productCode[货品条码],查询该货品条码是否存在于伯俊系统中
//@Param  productCode string
//@Return bool error
func (api *Api) CheckProductAlias(productCode string) error {
	ptd := api.burgeon.NewQuery()
	ptd.QuerySetTable("M_PRODUCT_ALIAS")
	ptd.QuerySetStartRange(0, 100)
	ptd.QuerySetCondition("NO", productCode)
	ptd.QuerySetResult("NO")
	jsonResultStr, err := api.burgeon.Post(ptd)
	if err == nil {
		rs := util.BurgeonRowsExists(jsonResultStr)
		if !rs {
			errmsg := "条码:" + productCode + "不存在"
			return errors.New(errmsg)
		} else {
			return nil
		}
	} else {
		return err
	}
}

//条码检测
//传入 productCodes[货品条码切片],查询这些货品条码是否存在于伯俊系统中
//若有一个不在,则返回 flase
//@Param  productCode string
//@Return bool error
func (api *Api) CheckProductAliasMore(productCodes []burgeon.ReTailItem) error {
	for _, v := range productCodes {
		err := api.CheckProductAlias(v.Name)
		if err != nil {
			return err
		}
	}
	return nil
}

//条码检测
//传入 productCodes[货品条码切片],查询这些货品条码是否存在于伯俊系统中
//若有一个不在,则返回 flase
//@Param  productCode string
//@Return bool error
func (api *Api) CheckProductStorange(storeCode string, products []burgeon.ReTailItem) error {
	for _, v := range products {
		//检查销售类型
		if v.Type == burgeon.ReTailBack {
			continue
		}
		num, err := api.GetStoreStorage(storeCode, v.Name)
		if err != nil {
			return err
		}
		if num < v.Qty {
			errmsg := fmt.Sprintf("%s的库存为:%d,销售数量为:%d,请修改数量", v.Name, num, v.Qty)
			return errors.New(errmsg)
		}
	}
	return nil
}

//检查对应店是否有对应营业员
//仅当该店存在该营业员返回true,其余都是false
//@parm store_code 店仓编号
//@parm user_code 营业员编号
func (api *Api) CheckUserStore(storeCode, userCode string) error {
	ptd := api.burgeon.NewQuery()
	ptd.QuerySetTable("HR_EMPLOYEE_STORE")
	ptd.QuerySetResult("HR_EMPLOYEE_ID;NO", "C_STORE_ID;CODE")
	ptd.QuerySetStartRange(0, 10)
	ptd.QuerySetConditionCombine("and")
	ptd.QuerySetConditionExpr1("C_STORE_ID;CODE", storeCode)
	ptd.QuerySetConditionExpr2("HR_EMPLOYEE_ID;NO", userCode)
	jsonResultStr, err := api.burgeon.Post(ptd)
	if err == nil {
		rs := util.BurgeonRowsExists(jsonResultStr)
		if !rs {
			errmsg := "店仓:" + storeCode + "中没有营业员:" + userCode
			return errors.New(errmsg)
		} else {
			return nil
		}
	} else {
		return err
	}
}

//营业员检测
//传入 user_code[营业员编号],查询该营业员是否存在于伯俊系统中
//@Param  user_code string
//@Return bool error
func (api *Api) CheckUser(userCode string) error {
	ptd := api.burgeon.NewQuery()
	ptd.QuerySetTable("HR_EMPLOYEE")
	ptd.QuerySetResult("NO")
	ptd.QuerySetStartRange(0, 10)
	ptd.QuerySetConditionCombine("and")
	ptd.QuerySetConditionExpr1("ISACTIVE", "Y")
	ptd.QuerySetConditionExpr2("NO", userCode)
	jsonResultStr, err := api.burgeon.Post(ptd)
	if err == nil {
		rs := util.BurgeonRowsExists(jsonResultStr)
		if !rs {
			errmsg := "营业员:" + userCode + "不存在"
			err = errors.New(errmsg)
		}
	}
	return err
}

//店仓检测
//传入 store_code[店仓编号],查询该店仓是否存在于伯俊系统中
//@Param  user_code string
//@Return bool error
func (api *Api) CheckStore(store_code string) error {
	ptd := api.burgeon.NewQuery()
	ptd.QuerySetTable("C_STORE")
	ptd.QuerySetResult("CODE")
	ptd.QuerySetStartRange(0, 10)
	ptd.QuerySetConditionCombine("and")
	ptd.QuerySetConditionExpr1("ISACTIVE", "Y")
	ptd.QuerySetConditionExpr2("CODE", store_code)
	jsonResultStr, err := api.burgeon.Post(ptd)
	if err == nil {
		rs := util.BurgeonRowsExists(jsonResultStr)
		if !rs {
			errmsg := store_code + "店仓不存在"
			err = errors.New(errmsg)
		}
	}
	return err
}

//rs0 条码有错
//rs1 店仓有错
//rs2 店员
//rs3 店员所属店仓
//rs4 店仓盘点日期
//rs5条码库存 是否满足
func (api *Api) CheckAll(store_code, user_code, retailData string, products []burgeon.ReTailItem) (rs0, rs1, rs2, rs3, rs4, rs5 error) {
	//检测店仓
	rs1 = api.CheckStore(store_code)
	if rs1 != nil {
		return
	}
	//检测店仓盘点日期
	rs4 = api.CheckStoreDateBlock(store_code, retailData)
	if rs4 != nil {
		return
	}
	//检测用户
	rs2 = api.CheckUser(user_code)
	if rs2 != nil {
		return
	}
	//检测用户所属店仓
	rs3 = api.CheckUserStore(store_code, user_code)
	if rs3 != nil {
		return
	}
	//检测条码
	rs0 = api.CheckProductAliasMore(products)
	if rs0 != nil {
		return
	}
	//检测条码库存
	rs5 = api.CheckProductStorange(store_code, products)
	return
}

//VIP检测
//传入 vipcard[vip卡号],查询该vip是否存在于伯俊系统中
//@Param  vipcard string
//@Return bool error
func (api *Api) CheckVip(vipcard string) (exist bool, err error) {
	ptd := api.burgeon.NewQuery()
	ptd.QuerySetTable("C_VIP")
	ptd.QuerySetStartRange(0, 100)
	ptd.QuerySetCondition("CARDNO", vipcard)
	ptd.QuerySetResult("CARDNO")
	jsonResultStr, err := api.burgeon.Post(ptd)
	if err == nil {
		return util.BurgeonRowsExists(jsonResultStr), nil
	} else {
		return false, err
	}
}

//VIP检测
//传入 vipcard[vip卡号],查询该vip是否存在于伯俊系统中
//@Param  vipcard string
//@Return bool error
func (api *Api) CheckStoreDateBlock(storCode, retailData string) (err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("C_STORE")
	pts.QuerySetCondition("CODE", storCode)
	pts.QuerySetResult("DATEBLOCK")
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	} else {
		dateBlock := gjson.Get(js_string, "0.rows.0.0").String()
		if dateBlock == "" {
			//没得盘点日期
			return errors.New("店仓盘点日期为空,不予以处理")
		} else {
			retailDataNum, _ := strconv.Atoi(retailData)
			dateBlockNum, _ := strconv.Atoi(dateBlock)
			if dateBlockNum < retailDataNum {
				return nil
			} else {
				errmsg := fmt.Sprintf("店仓[%s]盘点日期大于等于单据日期", storCode)
				return errors.New(errmsg)
			}
		}
	}
}

//通过店仓库存查询
func (api *Api) GetStoreStorage(storId string, productCode string) (qty int64, err error) {
	ptd := api.burgeon.NewQuery()
	ptd.QuerySetTable("V_FA_V_STORAGE1")
	ptd.QuerySetResult("QTY")
	ptd.QuerySetStartRange(0, 10)
	ptd.QuerySetConditionCombine("and")
	ptd.QuerySetConditionExpr1("C_STORE_ID;CODE", storId)
	ptd.QuerySetConditionExpr2("M_PRODUCTALIAS_ID;NO", productCode)
	js_string, err := api.burgeon.Post(ptd)
	if err != nil {
		return 0, err
	} else {
		qty = gjson.Get(js_string, "0.rows.0.0").Int()
		return

	}
}
