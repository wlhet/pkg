package api

import (
	"gitee.com/wlhet/pkg/burgeon"

	"gitee.com/wlhet/pkg/burgeon/util"

	"gitee.com/wlhet/pkg/gjson"
)

//获取营业员信息 工号 姓名  店仓  进销商
//@param nunber string 营业员编号
//@return burgeon.Employee 营业员详细信息[工号 姓名  店仓  进销商]
func (api *Api) GetemployeesInfo(nunber string) (employee burgeon.Employee, err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("C_V_EMPLOYEE")
	pts.QuerySetCondition("NO", nunber)
	pts.QuerySetResult("NAME", "NO", "C_STORE_ID;NAME", "C_CUSTOMER_ID;NAME")
	var emp burgeon.Employee
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return emp, err
	} else {
		emp.Name = gjson.Get(js_string, "0.rows.0.0").String()
		emp.WorkNo = gjson.Get(js_string, "0.rows.0.1").String()
		emp.Store = gjson.Get(js_string, "0.rows.0.2").String()
		emp.Customer = gjson.Get(js_string, "0.rows.0.3").String()
		return emp, nil
	}
}

//获取营业员 ID NAME
//@param nunber string 营业员编号
//@return burgeon.Employee 营业员详细信息[ID NAME]
func (api *Api) GetemployeesIdName(nunber string) (string, string, error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("C_V_EMPLOYEE")
	pts.QuerySetCondition("NO", nunber)
	pts.QuerySetResult("ID", "NAME")
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return "", "", err
	} else {
		return gjson.Get(js_string, "0.rows.0.0").String(), gjson.Get(js_string, "0.rows.0.1").String(), nil
	}
}

//获取营业员所有所属店仓
//@param nunber string 营业员编号
//@return []string 营业员所在店仓名字,可为多个店仓
func (api *Api) GetemployeesStore(nunber string) (stores []string, err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("HR_EMPLOYEE_STORE")
	pts.QuerySetCondition("HR_EMPLOYEE_ID;NO", nunber)
	pts.QuerySetResult("C_STORE_ID;NAME")
	store := make([]string, 0)
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return store, err
	} else {
		array := gjson.Get(js_string, "0.rows.#.0").Array()
		for _, v := range array {
			store = append(store, v.String())
		}
		return store, nil
	}
}

//新增员工信息
//@param nunber string 营业员编号
//@param name   string 营业员姓名
//@param store  string 店仓编号或者店仓名
//@param customer string 经销商编号或经销商名
//@param phone string 营业员手机号
//@return err   error  是否成功新增
func (api *Api) AddEmployees(nunber, name, store, customer, phone string) error {
	pts := api.burgeon.NewObjectCreate()
	pts.ObjectCreateSetTable("C_V_EMPLOYEE")
	pts.ObjectCreateSetColumn("NO", nunber)
	pts.ObjectCreateSetColumn("NAME", name)
	pts.ObjectCreateSetColumn("C_STORE_ID__NAME", store)
	pts.ObjectCreateSetColumn("C_CUSTOMER_ID__NAME", customer)
	pts.ObjectCreateSetColumn("HANDSET", phone)
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}
