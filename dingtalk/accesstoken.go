package dingtalk

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

//获取Token
func (dt *DingTalk) GetToken() string {
	timeNow := time.Now()
	if timeNow.After(time.Unix(dt.ExpiresIn, 0)) {
		dt.mu.Lock()
		defer dt.mu.Unlock()
		tk, err := dt.getTokenFromServer()
		if err != nil {
			log.Fatalln(err)
			return EmptyString
		}
		nowTime := time.Now().Unix()
		dt.AccessToken = tk.Token.AccessToken
		dt.ExpiresIn = tk.ExpiresIn + nowTime
		return dt.AccessToken
	} else {
		return dt.AccessToken
	}
}

//发送网络请求
func (dt *DingTalk) getTokenFromServer() (AccessTokenResponse, error) {
	client := &http.Client{}
	var accessTokenResp AccessTokenResponse
	FullUrl := fmt.Sprintf(getTokenUrl, dt.AppKey, dt.AppSecret)
	request, _ := http.NewRequest("GET", FullUrl, nil)
	response, err := client.Do(request)
	if err != nil {
		log.Fatalln(err)
		return accessTokenResp, err
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatalln(err)
		return accessTokenResp, err
	}
	err = json.Unmarshal(body, &accessTokenResp)
	if err != nil {
		log.Fatalln(err)
		return accessTokenResp, err
	}
	return accessTokenResp, nil
}
