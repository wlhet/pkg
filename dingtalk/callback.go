package dingtalk

import "fmt"

//注册回调
func (dt *DingTalk) RegCallBack(cb CallBack) BaseResponse {
	access_token := dt.GetToken()
	api_url := fmt.Sprintf(regCallBackUrl, access_token)
	var msg BaseResponse
	PostJsonPtr(api_url, cb, &msg)
	return msg
}

//更新回调
func (dt *DingTalk) UpdateCallBack(cb CallBack) BaseResponse {
	access_token := dt.GetToken()
	api_url := fmt.Sprintf(updateCallBackUrl, access_token)
	var msg BaseResponse
	PostJsonPtr(api_url, cb, &msg)
	return msg
}

//获取回调
func (dt *DingTalk) GetCallBack() CallBackResponse {
	var cb CallBackResponse
	access_token := dt.GetToken()
	api_url := fmt.Sprintf(getCallBackUrl, access_token)
	GetJson(api_url, &cb)
	return cb
}

//删除回调
func (dt *DingTalk) DeleteCallBack() BaseResponse {
	var msg BaseResponse
	access_token := dt.GetToken()
	api_url := fmt.Sprintf(deleteCallBackUrl, access_token)
	GetJson(api_url, &msg)
	return msg
}
