package dingtalk

import (
	"crypto/cipher"
	"sync"
)

type DingTalkStore struct {
	store map[string]*DingtalkAndCrypto
	mu    *sync.Mutex
}
type DingtalkAndCrypto struct {
	d *DingTalk
	c *Crypto
}

type DingTalk struct {
	CorpId    string
	AppKey    string //账号
	AppSecret string //密钥
	AgentId   string //appID
	Token
	mu *sync.Mutex
}
type BaseResponse struct {
	ErrMsg    string `json:"errmsg"`
	ErrCode   int    `json:"errcode"`
	RequestId string `json:"request_id"`
}

type Token struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
}
type Crypto struct {
	Token    string
	AesKey   string
	SuiteKey string
	block    cipher.Block
	bkey     []byte
}

//回调信息
type CallBack struct {
	CallBackTag []string `json:"call_back_tag"`
	Token       string   `json:"token"`
	AesKey      string   `json:"aes_key"`
	Url         string   `json:"url"`
}

//回调信息返回消息
type CallBackResponse struct {
	CallBack
	BaseResponse
}

//审批事件结构体
type BpmsEvent struct {
	EventType         string `json:"EventType"`         //事件类型
	ProcessInstanceId string `json:"processInstanceId"` //审批实例id
	CorpId            string `json:"corpId"`            //审批实例对应的企业
	CreateTime        int64  `json:"createTime"`        //实例创建时间。
	Title             string `json:"title"`             //实例标题
	Type              string `json:"type"`              //类型，type为start表示审批实例开始 /finish：审批正常结束（同意或拒绝 terminate：审批终止（发起人撤销审批单）
	StaffId           string `json:"staffId"`           //发起审批实例的员工
	Url               string `json:"url"`               //审批实例url，可在钉钉内跳转到审批页面
	BizCategoryId     string `json:"bizCategoryId"`     //审批实例对应表单类别
	FinishTime        int64  `json:"finishTime"`        //审批结束时间
	Result            string `json:"result"`            //redirect
	Remark            string `json:"remark"`            //remark表示操作时写的评论内容
	TaskId            int64  `json:"taskId"`            //任务ID
	Content           string `json:"content"`           //内容
	ProcessCode       string `json:"processCode"`       //审批模板的唯一码
	BusinessId        string `json:"businessId"`        //咱不知道
}

//获取token返回信息
type AccessTokenResponse struct {
	BaseResponse
	Token
}

type GetUsersNumResponse struct {
	BaseResponse
	Result struct {
		Count int64 `json:"count"`
	} `json:"result"`
}

//手机号获取用户Id返回信息
type GeIdByMobileResponse struct {
	BaseResponse
	Result struct {
		UserId string `json:"userid"`
	} `json:"result"`
}

//获取用户基本信息返回消息
type UserInfoResponse struct {
	BaseResponse
	UserInfo `json:"result"`
}

//{16026378102273899
//gyNOiSb3EDWhKBb8kIeKWLAiEiE
//张原
//https://static-legacy.dingtalk.com/media/lADPD3lGrynCTsnNCRTNCRA_2320_2324.jpg
//18570393066
// 20101102
// 店员   [357212935] {} 1604246400000 true true false false false [{357212935 false}] []}}
//用户基本信息
type UserInfo struct {
	UserId       string         `json:"userid"`
	Unionid      string         `json:"unionid,omitempty"`
	Name         string         `json:"name,omitempty"`
	Avatar       string         `json:"avatar,omitempty"` //头像
	Mobile       string         `json:"mobile,omitempty"`
	JobNumber    string         `json:"job_number,omitempty"` //工号
	Title        string         `json:"title,omitempty"`      //职位
	Email        string         `json:"email,omitempty"`
	Remark       string         `json:"remark,omitempty"`
	DeptIdList   []int64        `json:"dept_id_list,omitempty"`
	Extension    string         `json:"extension,omitempty"`   //爱好 {"爱好":"旅游","年龄":"24"}
	HiredDate    int64          `json:"hired_date,omitempty"`  //入职时间戳
	Active       bool           `json:"active,omitempty"`      //是否激活钉钉
	RealAuthed   bool           `json:"real_authed,omitempty"` //是否实名
	Senior       bool           `json:"senior,omitempty"`      //是否高管
	Admin        bool           `json:"admin,omitempty"`       //是否管理员
	Boss         bool           `json:"boss,omitempty"`        //是否老板
	LeaderInDept []LeaderInDept `json:"leader_in_dept,omitempty"`
	RoleList     []RoleList     `json:"role_list,omitempty"`
}

type LeaderInDept struct {
	DeptId int  `json:"dept_id,omitempty"`
	Leader bool `json:"leader,omitempty"`
}
type RoleList struct {
	Id        int    `json:"id,omitempty"`
	Name      string `json:"name,omitempty"`
	GroupName string `json:"group_name,omitempty"`
}

//更新用户返回信息
type UpdateUserInfoResponse struct {
	BaseResponse
}

//新建用户返回信息
type CreateUserResponse struct {
	BaseResponse
	Result struct {
		UserId string `json:"userid"`
	} `json:"result"`
}

type ResultAndSuccess struct {
	Result  bool `json:"result"`  //终止成功
	Success bool `json:"success"` //调用成功
}

//终止审批实例返回信息
type StopProInstanceResponse struct {
	BaseResponse
	ResultAndSuccess
}

//终止审批实例返回信息
type CommitAddInstanceResponse struct {
	BaseResponse
	ResultAndSuccess
}

//用户登录Code
type UserLoginByCodeResponse struct {
	BaseResponse
	UserId   string `json:"userid"`
	Name     string `json:"name"`
	DeviceId string `json:"deviceid"`
	IsSys    bool   `json:"is_sys"`
	SysLevel int64  `json:"sys_level"`
}

//Department

type Department struct {
	DeptId                int64    `json:"dept_id"`                  //部门ID
	Name                  string   `json:"name"`                     //部门名称
	ParentId              int64    `json:"parent_id"`                //父部门ID
	SourceIdentifier      string   `json:"source_identifier"`        //未知
	CreateDeptGroup       bool     `json:"create_dept_group"`        //是否自动创建部门群
	AutoAddUser           bool     `json:"auto_add_user"`            //成员是否自动加入部门群
	FromUnionOrg          bool     `json:"from_union_org"`           //未知
	Tags                  string   `json:"tags"`                     //部门标签
	Order                 int64    `json:"order"`                    //部门排序
	DeptGroupChatId       int64    `json:"dept_group_chat_id"`       //部门群ID
	GroupContainSubDept   bool     `json:"group_contain_sub_dept"`   //部门群是否包含子部门
	OrgDeptOwner          int64    `json:"org_dept_owner"`           //企业群群主ID
	DeptManagerUseridList []string `json:"dept_manager_userid_list"` //部门的主管列表
	OuterDept             bool     `json:"outer_dept"`               //是否限制本部门成员查看通讯录
	OuterPermitDepts      []int64  `json:"outer_permit_depts"`       //当限制部门成员的通讯录查看范围时（即outer_dept为true时），配置的部门员工可见部门列表
	OuterPermitUsers      []string `json:"outer_permit_users"`       //当限制部门成员的通讯录查看范围时（即outer_dept为true时），配置的部门员工可见员工列表
	HideDept              bool     `json:"hide_dept"`                //是否隐藏本部门
	UserPermits           []string `json:"user_permits"`             //当隐藏本部门时（即hide_dept为true时），配置的允许在通讯录中查看本部门的员工列表
	DeptPermits           []int64  `json:"dept_permits"`             //当隐藏本部门时（即hide_dept为true时），配置的允许在通讯录中查看本部门的部门列表
}

//创建部门
type DepartmentCreateResponse struct {
	BaseResponse
	Result struct {
		Dept_id int64 `json:"dept_id"`
	} `json:"result"`
}

//获取部门详情
type DepartmentGetResponse struct {
	BaseResponse
	Department `json:"result"`
}

//查询子部门ID
type DepartmentListsubidResponse struct {
	BaseResponse
	Result struct {
		DeptIdList []int64 `json:"dept_id_list"`
	} `json:"result"`
}

type DepartmentListsubResponse struct {
	BaseResponse
	Result []Department `json:"result"`
}

type ProcessInstanceResponse struct {
	BaseResponse
	RequestId       string `json:"request_id"`
	ProcessInstance `json:"process_instance"`
}

type ProcessInstance struct {
	Title                      string   `json:"title"`
	CreateTime                 int64    `json:"create_time"`
	FinishTime                 int64    `json:"finish_time"`
	OriginatorUserId           string   `json:"originator_userid"`             //发起人ID
	OriginatorDeptId           string   `json:"originator_dept_id"`            //发起人部门   -1跟部门
	Status                     string   `json:"status"`                        //状态码 NEW：新创建 RUNNING：审批中 TERMINATED：被终止 COMPLETED：完成 CANCELED：取消
	ApproverUserids            []string `json:"approver_userids"`              //审批人ID素组
	CcUserids                  []string `json:"cc_userids"`                    //抄送人ID数组
	Result                     string   `json:"result"`                        //结果
	BusinessId                 string   `json:"business_id"`                   //审批实例业务编号。
	AttachedProcessInstanceIds []string `json:"attached_process_instance_ids"` //附属实例
	BizAction                  string   `json:"biz_action"`                    //MODIFY：表示该审批实例是基于原来的实例修改而来 REVOKE：表示该审批实例是由原来的实例撤销后重新发起的 NONE表示正常发起
	OriginatorDeptName         string   `json:"originator_dept_name"`          //发起部门
	FormComponentValues        []struct {
		Name          string `json:"name"`
		Value         string `json:"value"`
		Id            string `json:"id"`
		ComponentType string `json:"component_type"`
	} `json:"form_component_values"`
}

type ProcessCodeResponse struct {
	BaseResponse
	ProcessCode string `json:"process_code"`
}

type DepartmentListParentByUserResponse struct {
	BaseResponse
	Result struct {
		ParentList []struct {
			ParentDeptIdList []int64 `json:"parent_dept_id_list"`
		} `json:"parent_list"`
	} `json:"result"`
}

//部门ID获取上级部门列表响应
type DepartmentListParentByDepResponse struct {
	BaseResponse
	Result struct {
		ParentDeptIdList []int64 `json:"parent_id_list"`
	} `json:"result"`
}
