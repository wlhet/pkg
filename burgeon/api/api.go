package api

import (
	"gitee.com/wlhet/pkg/burgeon"
)

var DefaultBurgeonServers map[string]*Api
var MPServers map[string][]string

/*
this package is the main api of burgeon;
all public api are in this package

how to use:
	bg:=burgeon.NewBurgeonConnection(AppId, AppKey, ApiUrl)
	api:=api.NewApi(bg)
	api.Func()
*/

type Api struct {
	burgeon *burgeon.BurgeonConnection
}

//@Param  bg *burgeon.BurgeonConnection
//@Return *Api
func NewApi(appId, appKey, apiUrl string) *Api {
	return &Api{burgeon: burgeon.NewBurgeonConnection(appId, appKey, apiUrl)}
}

func RegDefaultBurgeonServers(svs map[string]*Api) {
	DefaultBurgeonServers = svs
}
func RegMPServers(mps map[string][]string) {
	MPServers = mps
}
