package burgeon

import (
	"encoding/json"
	"errors"
	"time"

	"gitee.com/wlhet/pkg/burgeon/util"

	"gitee.com/wlhet/pkg/httplib"
)

// var NewVipCardInfo *VipCardConfig

type BurgeonConnection struct {
	AppId  string
	AppKey string
	ApiUrl string
}

type PostData struct {
	Id      int                    `json:"id"`      //id
	Command string                 `json:"command"` //操作类型
	Params  map[string]interface{} `json:"params"`  //所需参数  json
}

//向外单独提供
func NewBurgeonConnection(appId, appKey, apiUrl string) *BurgeonConnection {
	return &BurgeonConnection{appId, appKey, apiUrl}
}

//用于初始化包内默认连接对象
// func InitBurgeonConnection(AppId, AppKey, ApiUrl string) {
// 	baseConnection = &BurgeonConnection{
// 		AppId:  AppId,
// 		AppKey: AppKey,
// 		ApiUrl: ApiUrl,
// 	}
// }

// func (bg *BurgeonConnection) InitVipCardConfig(viptype, customer, store, area, validdate string) {
// 	NewVipCardInfo = &VipCardConfig{
// 		VipType:      viptype,
// 		Customer:     customer,
// 		Store:        store,
// 		IntegralArea: area,
// 		ValidDate:    validdate,
// 	}
// }

//Burgeon--------------------------------------------------------------------------
//APP_KEY_2 + tmp + app_ser_md5s
//将密码MD5后和账号时间戳按照 账号 时间戳 密码MD5 拼接 返回
func (bg *BurgeonConnection) GetSign() (string, string) {
	tmp := bg.GetTimeMillisecondString()
	md5key := StringToMd5(bg.AppKey)
	return tmp, StringToMd5(bg.AppId + tmp + md5key)
}

//2020-09-26 15:06:23.000
func (bg *BurgeonConnection) GetTimeMillisecondString() string {
	tmp := time.Now().Format("2006-01-02 15:04:05.000")
	return tmp
}

//封装了请求提交函数  只需传入 transactions即可  返回json字符串
func (bg *BurgeonConnection) Post(comm ...PostData) (string, error) {
	dataJsonByte, _ := json.Marshal(&comm)
	dataJsonStr := string(dataJsonByte)
	tmp, sign := bg.GetSign()
	req := httplib.Post(bg.ApiUrl)
	req.Param("sip_appkey", bg.AppId)
	req.Param("sip_timestamp", tmp)
	req.Param("sip_sign", sign)
	req.Param("transactions", dataJsonStr)
	response, err := req.String()
	if err != nil {

		return EmptyString, err
	}
	code, errmsg := util.GetBurgeonCodeAndMessage(response)
	if code == 0 {
		return response, nil
	} else {
		return EmptyString, errors.New(errmsg)
	}
}
