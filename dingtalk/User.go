package dingtalk

import (
	"fmt"
)

//获取企业人数
//@parmas only_active bool   1.false：包含未激活钉钉的人员数量 2.true：只包含激活钉钉的人员数量
func (dt *DingTalk) GetUsersNum(only_active bool) int64 {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(getUsersNumUrl, token)
	PostData := map[string]bool{"only_active": only_active}
	var msg GetUsersNumResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err == nil {
		return msg.Result.Count
	} else {
		return -1
	}
}

//根据手机号获取userid
//@parmas mobile string
func (dt *DingTalk) GetIdByMobile(m string) string {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(geIdByMobileUrl, token)
	PostData := map[string]string{"mobile": m}
	var msg GeIdByMobileResponse

	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err == nil {
		return msg.Result.UserId
	} else {
		return ""
	}
}

//通过UserId获取用户信息
func (dt *DingTalk) GetUserInfo(userid string, language ...string) UserInfoResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(getUserUrl, token)
	PostData := map[string]string{"userid": userid}
	for _, v := range language {
		if v == "zh_CN" || v == "en_US" {
			PostData["language"] = v
		}
	}
	var msg UserInfoResponse
	PostJsonPtr(apiUrl, PostData, &msg)
	return msg
}

//通过UserId获取用户信息
func (dt *DingTalk) UpdateUserInfo(info UserInfo) UpdateUserInfoResponse {
	var msg UpdateUserInfoResponse
	if info.UserId == "" {
		msg.ErrCode = -1000
		msg.ErrMsg = "请传入ID"
		return msg
	}
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(updateUserUrl, token)
	PostJsonPtr(apiUrl, info, &msg)
	return msg
}

//新增用户  ID不传的话自动生成
func (dt *DingTalk) CreateUser(info UserInfo) CreateUserResponse {
	var msg CreateUserResponse
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(createUserUrl, token)
	PostJsonPtr(apiUrl, info, &msg)
	return msg
}

//code免登
func (dt *DingTalk) UserLoginByCode(code string) UserLoginByCodeResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(getUserInfoByCodeUrl, token, code)
	var msg UserLoginByCodeResponse
	GetJson(apiUrl, &msg)
	return msg
}
