package api

import (
	"fmt"

	"gitee.com/wlhet/pkg/burgeon/util"

	"gitee.com/wlhet/pkg/gjson"
)

//获取店仓日结日期
func (api *Api) GetStoreChkDay(store string) (chkday string, err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("C_STORE")
	pts.QuerySetCondition("CODE", store)
	pts.QuerySetResult("CHKDAY")
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		if dateString == "null" {
			return "", nil
		} else {
			return dateString, nil
		}

	}
}

//修改店仓日结
func (api *Api) ChangeStoreChkDay(storeId string, dateString string) error {
	pts := api.burgeon.NewObjectModify()
	pts.ObjectModifySetTable("C_STORE")
	pts.ObjectModifySetColumn("id", storeId)
	pts.ObjectModifySetColumn("CHKDAY", dateString)
	pts.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}

//获取店仓盘点日期
func (api *Api) GetStoreDateBlock(storeCode string) (chkday string, err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("C_STORE")
	pts.QuerySetCondition("CODE", storeCode)
	pts.QuerySetResult("DATEBLOCK")
	js_string, err := api.burgeon.Post(pts)
	fmt.Println(js_string)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		if dateString == "null" {
			return "", nil
		} else {
			return dateString, nil
		}

	}
}

//修改店仓盘点日期
func (api *Api) ChangeStoreDateBlock(storeId string, dateString string) error {
	pts := api.burgeon.NewObjectModify()
	pts.ObjectModifySetTable("C_V_STORE_OTH")
	pts.ObjectModifySetColumn("id", storeId)
	pts.ObjectModifySetColumn("DATEBLOCK", dateString)
	pts.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}

//通过店仓编码获取店仓Id
func (api *Api) GetStoreIdByCode(storCode string) (storeId string, err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("C_STORE")
	pts.QuerySetCondition("CODE", storCode)
	pts.QuerySetResult("ID")
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		return dateString, nil

	}
}

//通过店仓编码获取店仓Id,日结日期,盘点日期
func (api *Api) GetStoreIdChkDayDateBlock(storCode string) (storeId, chkDay, dateBlock string, err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("C_STORE")
	pts.QuerySetCondition("CODE", storCode)
	pts.QuerySetResult("ID", "CHKDAY", "DATEBLOCK")
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return EmptyString, EmptyString, EmptyString, err
	} else {
		storeId = gjson.Get(js_string, "0.rows.0.0").String()
		chkDay = gjson.Get(js_string, "0.rows.0.1").String()
		dateBlock = gjson.Get(js_string, "0.rows.0.2").String()
		return

	}
}
