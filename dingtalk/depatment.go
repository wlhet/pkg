package dingtalk

import (
	"fmt"
	"log"
	"sync"
)

//创建部门
func (dt *DingTalk) DepartmentCreate(name string, parent_id int64) DepartmentCreateResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentCreateUrl, token)
	PostData := map[string]interface{}{
		"name":      name,      //部门名字
		"parent_id": parent_id, //父部门ID
	}
	var msg DepartmentCreateResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg
}

// /更新部门  仅限更新名字和父级部门
func (dt *DingTalk) DepartmentUpdate(name string, dept_id, parent_id int64) BaseResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentUpdateUrl, token)
	PostData := map[string]interface{}{
		"dept_id":   dept_id,
		"name":      name,      //部门名字
		"parent_id": parent_id, //父部门ID
	}
	var msg BaseResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg
}

// /删除部门
func (dt *DingTalk) DepartmentDelete(dept_id int64) BaseResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentDeleteUrl, token)
	PostData := map[string]interface{}{
		"dept_id": dept_id,
	}
	var msg BaseResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg
}

// 获取部门详情
func (dt *DingTalk) DepartmentGet(dept_id int64) DepartmentGetResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentGetUrl, token)
	PostData := map[string]interface{}{
		"dept_id": dept_id,
	}
	var msg DepartmentGetResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg
}

//获取子部门ID列表
func (dt *DingTalk) DepartmentListsubid(dept_id int64) []int64 {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentListsubidUrl, token)
	PostData := map[string]interface{}{
		"dept_id": dept_id,
	}
	var msg DepartmentListsubidResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg.Result.DeptIdList
}

//获取所有部门ID
func (dt *DingTalk) GetAllChildDepId(todo []int64, done []int64) []int64 {
	if done == nil {
		done = make([]int64, 0)
	}
	tmp := make([]int64, 0)
	for _, v := range todo {
		rs := dt.DepartmentListsubid(v)
		// fmt.Println(rs)
		tmp = append(tmp, rs...)
	}
	done = append(done, todo...)
	todo = tmp
	if len(todo) > 0 {
		return dt.GetAllChildDepId(todo, done)
	} else {
		return done
	}
}

func (dt *DingTalk) GetAllChildDepId2() []int64 {
	rs := dt.DepartmentListsubid(1)
	depChan := make(chan int64, 5)
	wg := sync.WaitGroup{}
	result := make([]int64, 0)
	for _, v := range rs {
		dep := v
		wg.Add(1)
		go func(dep int64, depchan chan int64) {
			rs1 := dt.GetAllChildDepId([]int64{dep}, nil)
			for _, v1 := range rs1 {
				depChan <- v1
			}
			wg.Done()
		}(dep, depChan)
	}

	go func(ch chan int64) {
		for r := range ch {
			fmt.Println("receve", r)
			if r == 0 {
				break
			}
			result = append(result, r)
		}
	}(depChan)
	wg.Wait()
	close(depChan)
	return result
}

//获取所有部门ID

//获取部门列表 dept_id=1 获取所有部门
func (dt *DingTalk) DepartmentListsub(dept_id int64) DepartmentListsubResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentListsubUrl, token)
	PostData := map[string]interface{}{
		"dept_id": dept_id,
	}
	var msg DepartmentListsubResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg
}

//POST https://oapi.dingtalk.com/topapi/v2/department/listparentbyuser?access_token=ACCESS_TOKEN
//通过用户ID获取所有父部门ID
func (dt *DingTalk) DepartmentListParentByUser(uid string) DepartmentListParentByUserResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentListparentbyuserUrl, token)
	PostData := map[string]interface{}{
		"userid": uid,
	}
	var msg DepartmentListParentByUserResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg
}

//POST POST https://oapi.dingtalk.com/topapi/v2/department/listparentbydept?access_token=ACCESS_TOKEN
//通过部门ID获取所有父部门ID
func (dt *DingTalk) DepartmentListParentByDep(depid int64) DepartmentListParentByDepResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(departmentListparentbydeptUrl, token)
	PostData := map[string]interface{}{
		"dept_id": depid,
	}
	var msg DepartmentListParentByDepResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err != nil {
		log.Fatalln(err)
	}
	return msg
}
