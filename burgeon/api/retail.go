package api

import (
	"crypto/rand"
	"errors"
	"math/big"
	"time"

	"gitee.com/wlhet/pkg/burgeon"

	"gitee.com/wlhet/pkg/burgeon/util"

	"gitee.com/wlhet/pkg/gjson"
)

/*

	bg := burgeon.NewBurgeonConnection("nea@burgeon.com.cn", "longda2018", "http://qt.imix7.com:90/servlets/binserv/Rest")
	rs, _ := bg.AddNewMRetail("9999", "9999", "测试", "20210318",
		burgeon.ReTailItem{Name: "723763288132", Price: 1000.0, Qty: 10, IsSwap: "N", Type: 2},
		burgeon.ReTailItem{Name: "723763291965", Price: 1000.0, Qty: 10, IsSwap: "N", Type: 1})
	fmt.Println(rs)

*/

//新增零售单
//@param store       string 店仓编号或者店仓名
//@param saler       string 营业员编号
//@param remark      string 零售单备注
//@param billdate    string 零售单日期
//@param retailitems []burgeon.ReTailItem 零售行明细
//@param payitems    []burgeon.PayItem    零售付款方式
//@return retail     string 成功返回单据编号,失败返回空串
func (api *Api) AddNewMRetail(submit bool, store, saler, remark, billdate string, retailitems []burgeon.ReTailItem, payitems []burgeon.PayItem) (retail string, err error) {
	pst := api.burgeon.NewProcessOrder()
	pst.ProcessOrderMasterObjSetTable("M_RETAIL")
	pst.ProcessOrderIfSubmit(submit)
	pst.ProcessOrderMasterObjSetColumn("C_STORE_ID__NAME", store)
	pst.ProcessOrderMasterObjSetColumn("SALESREP_ID__NO", saler)
	pst.ProcessOrderMasterObjSetColumn("ISINTL", "N")
	pst.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark)
	pst.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	list := make([]map[string]interface{}, 0)
	if len(retailitems) == 0 {
		return EmptyString, errors.New("NowItemsErrr")
	}
	for _, v := range retailitems {
		list = append(list, map[string]interface{}{
			"M_PRODUCT_ID__NAME": v.Name,
			"PRICEACTUAL":        v.Price,
			"QTY":                v.Qty,
			"TYPE":               v.Type,
			"IS_SWAP":            v.IsSwap,
		})
	}
	pst.ProcessOrderDetailObjsAddrefobjs("M_RETAILITEM", list...)
	if len(payitems) > 0 { //有付款方式,需要增加付款方式子表头
		pst.ProcessOrderDetailObjsSetTables("M_RETAILITEM", "M_RETAILPAYITEM") //子表列表中增付款方式表 注意有顺序限制
		listPay := make([]map[string]interface{}, 0)
		for _, v := range payitems {
			listPay = append(listPay, map[string]interface{}{
				"C_PAYWAY_ID__NAME": v.Type,
				"BASE_PAYAMOUNT":    0,
				"PAYAMOUNT":         v.PayAmt,
			})
		}
		pst.ProcessOrderDetailObjsAddrefobjs("M_RETAILPAYITEM", listPay...)
	} else { //没有付款方式,只处理零售明细即可
		pst.ProcessOrderDetailObjsSetTables("M_RETAILITEM")
	}
	jsResponse, err := api.burgeon.Post(pst)
	if err != nil {
		switch err.Error() {
		case "行 1: 输入的数据已存在:店仓+创建时间+POS零售单号":
			randint, _ := rand.Int(rand.Reader, big.NewInt(5))
			time.Sleep(time.Duration(randint.Int64()) * time.Second)
			return api.AddNewMRetail(submit, store, saler, remark, billdate, retailitems, payitems)
		default:
			return "", err
		}
	}
	if submit { //提交直接返回单号
		_, retail = util.GetBurgeonCodeAndMessage(jsResponse)
	} else { //未提交需要自行读取单号
		objId := gjson.Get(jsResponse, "0.objectid").String()
		retail, _ = api.GetrRetailNoById(objId)
	}

	return retail, nil

}

//通过零售单Id获取单号
func (api *Api) GetrRetailNoById(id string) (no string, err error) {
	pts := api.burgeon.NewQuery()
	pts.QuerySetTable("M_RETAIL")
	pts.QuerySetCondition("ID", id)
	pts.QuerySetResult("DOCNO")
	js_string, err := api.burgeon.Post(pts)
	if err != nil {
		return EmptyString, err
	} else {
		dateString := gjson.Get(js_string, "0.rows.0.0").String()
		if dateString == "null" {
			return "", nil
		} else {
			return dateString, nil
		}

	}
}

//新增零售单 针对于开启多营业员的情况  还是只填写一个营业员,只是多了几个字段
//@param store       string 店仓编号或者店仓名
//@param saler       string 营业员编号
//@param remark      string 零售单备注
//@param billdate    string 零售单日期
//@param retailitems []burgeon.ReTailItem 零售行明细
//@param payitems    []burgeon.PayItem    零售付款方式
//@return retail     string 成功返回单据编号,失败返回空串
func (api *Api) AddNewMRetailWithOpenMoreSaler(submit bool, store, saler, remark, billdate string, retailitems []burgeon.ReTailItem, payitems []burgeon.PayItem) (retail string, err error) {
	pst := api.burgeon.NewProcessOrder()
	pst.ProcessOrderMasterObjSetTable("M_RETAIL")
	pst.ProcessOrderIfSubmit(submit)
	pst.ProcessOrderMasterObjSetColumn("C_STORE_ID__NAME", store)
	pst.ProcessOrderMasterObjSetColumn("SALESREP_ID__NO", saler)
	pst.ProcessOrderMasterObjSetColumn("ISINTL", "N")
	pst.ProcessOrderMasterObjSetColumn("DESCRIPTION", remark)
	pst.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	list := make([]map[string]interface{}, 0)
	if len(retailitems) == 0 {
		return EmptyString, errors.New("NowItemsErrr")
	}
	id, name, _ := api.GetemployeesIdName(saler)
	for _, v := range retailitems {
		list = append(list, map[string]interface{}{
			"M_PRODUCT_ID__NAME": v.Name,
			"PRICEACTUAL":        v.Price,
			"QTY":                v.Qty,
			"TYPE":               v.Type,
			"IS_SWAP":            v.IsSwap,
			"SALESREPS_ID":       id,
			"SALESREPS_NAME":     name,
			"SALESREPS_RATE":     "1.0",
		})
	}

	pst.ProcessOrderDetailObjsAddrefobjs("M_RETAILITEM", list...)
	if len(payitems) > 0 { //有付款方式,需要增加付款方式子表头
		pst.ProcessOrderDetailObjsSetTables("M_RETAILITEM", "M_RETAILPAYITEM") //子表列表中增付款方式表 注意有顺序限制
		listPay := make([]map[string]interface{}, 0)
		for _, v := range payitems {
			listPay = append(listPay, map[string]interface{}{
				"C_PAYWAY_ID__NAME": v.Type,
				"BASE_PAYAMOUNT":    0,
				"PAYAMOUNT":         v.PayAmt,
			})
		}
		pst.ProcessOrderDetailObjsAddrefobjs("M_RETAILPAYITEM", listPay...)
	} else { //没有付款方式,只处理零售明细即可
		pst.ProcessOrderDetailObjsSetTables("M_RETAILITEM")
	}
	jsResponse, err := api.burgeon.Post(pst)
	if err != nil {
		switch err.Error() {
		case "行 1: 输入的数据已存在:店仓+创建时间+POS零售单号":
			randint, _ := rand.Int(rand.Reader, big.NewInt(5))
			time.Sleep(time.Duration(randint.Int64()) * time.Second)
			return api.AddNewMRetail(submit, store, saler, remark, billdate, retailitems, payitems)
		default:
			return "", err
		}
	}
	if submit { //提交直接返回单号
		_, retail = util.GetBurgeonCodeAndMessage(jsResponse)
	} else { //未提交需要自行读取单号
		objId := gjson.Get(jsResponse, "0.objectid").String()
		retail, _ = api.GetrRetailNoById(objId)
	}

	return retail, nil

}
