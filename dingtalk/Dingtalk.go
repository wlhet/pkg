package dingtalk

import (
	"sync"
)

var DefaultDingtalk *DingTalk

//初始化包默认的Dingtalk 方便后续导入其他包使用
func RegDefaultDingtalk(CorpId, AppKey, AppSecret, AgentId string) {
	DefaultDingtalk = NewDingtalk(CorpId, AppKey, AppSecret, AgentId)
}

func NewDingtalk(CorpId, AppKey, AppSecret, AgentId string) *DingTalk {
	dt := DingTalk{CorpId: CorpId, AppKey: AppKey, AppSecret: AppSecret, AgentId: AgentId, mu: new(sync.Mutex)}
	return &dt
}
