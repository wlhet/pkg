package dingtalk

import (
	"errors"
	"sync"
)

//TODO

var DefaultDingtalkStore *DingTalkStore

func (dts *DingTalkStore) GetToken(dtName string) string {
	dt, ok := dts.store[dtName]
	if !ok {
		return ""
	}
	return dt.d.GetToken()
}

func (dts *DingTalkStore) GetDingtalk(dtName string) *DingTalk {
	dt, ok := dts.store[dtName]
	if !ok {
		return nil
	}
	return dt.d
}

func (dts *DingTalkStore) GetCrypto(dtName string) *Crypto {
	dt, ok := dts.store[dtName]
	if !ok {
		return nil
	}
	return dt.c
}

func (dts *DingTalkStore) DecryptMsg(dtName, signature, timeStamp, nonce, secretStr string) (string, error) {
	ds, ok := dts.store[dtName]
	if !ok {
		return "", errors.New("KeyIsFail")
	}
	return ds.c.DecryptMsg(signature, timeStamp, nonce, secretStr)
}

func (dts *DingTalkStore) EncryptMsg(dtName, replyMsg, timeStamp, nonce string) (string, string, error) {
	ds, ok := dts.store[dtName]
	if !ok {
		return "", "", errors.New("KeyIsFail")
	}
	return ds.c.EncryptMsg(replyMsg, timeStamp, nonce)
}

func (dts *DingTalkStore) Add(key string, ds *DingtalkAndCrypto) {
	dts.mu.Lock()
	defer dts.mu.Unlock()
	dts.store[key] = ds
}

func (dts *DingTalkStore) Delete(key string) {
	dts.mu.Lock()
	defer dts.mu.Unlock()
	delete(dts.store, key)

}

func (dts *DingTalkStore) Number() int {
	return len(dts.store)
}

func (dts *DingTalkStore) IsEmpty() bool {
	l := len(dts.store)
	return l <= 0
}

func NewDingtalkStore() *DingTalkStore {
	return &DingTalkStore{store: make(map[string]*DingtalkAndCrypto), mu: new(sync.Mutex)}
}

func RegDefaultDingtalkStore() {
	DefaultDingtalkStore = &DingTalkStore{store: make(map[string]*DingtalkAndCrypto), mu: new(sync.Mutex)}
}
