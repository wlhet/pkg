package dingtalk

import (
	"gitee.com/wlhet/pkg/httplib"
)

var EmptyString = ""

// GetJson 发送GET请求解析json
func GetJson(uri string, v interface{}) error {
	client := httplib.Post(uri)
	return client.ToJSON(&v)
}

// GetBody 发送GET请求，返回body字节
func GetBody(uri string) ([]byte, error) {
	client := httplib.Post(uri)
	return client.Bytes()
}

// Min golang min int
func Min(first int, args ...int) int {
	for _, v := range args {
		if first > v {
			first = v
		}
	}
	return first
}

func PostJson(uri string, obj interface{}) ([]byte, error) {
	client, err := httplib.Post(uri).JSONBody(obj)
	if err != nil {
		return nil, err
	}
	return client.Bytes()

}

func PostJsonPtr(uri string, obj interface{}, result interface{}) error {
	client, err := httplib.Post(uri).JSONBody(obj)
	if err != nil {
		return err
	}
	err = client.ToJSON(&result)
	if err != nil {
		return err
	}
	return nil
}

func PostJsonStrPtr(uri string, js string, result interface{}, contentType ...string) (err error) {
	client := httplib.Post(uri).Body([]byte(js))
	client.Header("Content-Type", "application/json")
	if err != nil {
		return err
	}
	err = client.ToJSON(&result)
	if err != nil {
		return err
	}
	return nil
}
