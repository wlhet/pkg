package dingtalk

import (
	"fmt"
)

//https://ding-doc.dingtalk.com/document#/org-dev-guide/obtains-details-about-a-specified-approval-instance

func (dt *DingTalk) GetProInstance(pid string) ProcessInstanceResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(processinstanceUrl, token)
	PostData := map[string]string{"process_instance_id": pid}
	var msg ProcessInstanceResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err == nil {
		return msg
	} else {
		return msg
	}
}
func (dt *DingTalk) GetProInstance1(pid string) string {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(processinstanceUrl, token)
	PostData := map[string]string{"process_instance_id": pid}
	rs, err := PostJson(apiUrl, PostData)
	if err == nil {
		return string(rs)
	} else {
		return string(rs)
	}
}

//通过审批名字获取ID
func (dt *DingTalk) GetBpmProcessCodeByName(name string) string {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(getProNameUrl, token)
	PostData := map[string]string{"name": name}
	var msg ProcessCodeResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	if err == nil {
		return msg.ProcessCode
	} else {
		return ""
	}
}

func (dt *DingTalk) StopProInstance(pid string, remark string) StopProInstanceResponse {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(processinstanceTerminateUrl, token)
	PostData := map[string]interface{}{
		"request": map[string]interface{}{
			"is_system":           true,
			"process_instance_id": pid,
			"remark":              remark,
		},
	}
	var msg StopProInstanceResponse
	err := PostJsonPtr(apiUrl, PostData, &msg)
	// fmt.Printf("%-v", PostData)
	if err == nil {
		return msg
	} else {
		fmt.Println(4, err.Error())
		return msg
	}

}

//https://developers.dingtalk.com/document/app/add-an-approval-comment
//https://oapi.dingtalk.com/topapi/process/instance/comment/add
//添加审批评论
func (dt *DingTalk) InstanceCommentAdd(process_instance_id string, text string, userid string) (ok bool, err error) {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(processinstanceCommitAddUrl, token)
	PostData := map[string]interface{}{
		"request": map[string]interface{}{
			"process_instance_id": process_instance_id,
			"text":                text,
			"comment_userid":      userid,
		},
	}
	var msg CommitAddInstanceResponse
	err = PostJsonPtr(apiUrl, PostData, &msg)
	if err == nil {
		return msg.Success, nil
	} else {
		return false, err
	}
}
