package burgeon

/*

	bg := burgeon.NewBurgeonConnection("username", "password", "http://qt.imix7.com:90/servlets/binserv/Rest")
	rs, _ := bg.AddNewMRetail("9999", "9999", "测试", "20210318",
		burgeon.ReTailItem{Name: "723763288132", Price: 1000.0, Qty: 10, IsSwap: "N", Type: 2},
		burgeon.ReTailItem{Name: "723763291965", Price: 1000.0, Qty: 10, IsSwap: "N", Type: 1})
	fmt.Println(rs)

*/

type ReTailType int //零售类型
const EmptyString = ""
const (
	ReTailSell ReTailType = iota + 1 //正常零售
	ReTailBack                       //退货
	ReTailSend                       //赠品
)

//零售单行明细
type ReTailItem struct {
	Name   string     //条码
	Price  float64    //总金额
	Qty    int64      //总数量
	Type   ReTailType //销售类别 1正常 2退货
	IsSwap string     //是否换货
}

//营业员信息
type Employee struct {
	Name     string
	WorkNo   string
	Store    string
	Customer string
}

//付款明细
type PayItem struct {
	Type   string  //付款方式
	PayAmt float64 //付款金额
}

//会员卡基础信息
type VipCardConfig struct {
	VipType      string //"XXX会员卡"  会员卡类型
	Customer     string //"XXX" 经销商
	Store        string //"XXX公司仓" 所属仓
	ValidDate    string //"20301231" 过期时间
	IntegralArea string //"XXX区域" 积分区域
	VipBrandName string //Vip品牌名
	TkEndDate    string //新用户送的券的有效期
}

//构造函数

func NewVipCardConfig(vipType, customer, store, validDate, integralArea, vipBrandName, tkEndDate string) *VipCardConfig {
	return &VipCardConfig{vipType, customer, store, validDate, integralArea, vipBrandName, tkEndDate}
}
