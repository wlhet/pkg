package util

import (
	"errors"

	"gitee.com/wlhet/pkg/gjson"
)

//处理伯俊接口返回的rows数据
func GetBurgeonRows() {

}

//获取伯俊Api返回的json数据的状态码及信息
func GetBurgeonCodeAndMessage(js_string string) (code int64, errmsg string) {
	code = gjson.Get(js_string, "0.code").Int()
	errmsg = gjson.Get(js_string, "0.message").String()
	return
}

//判断伯俊接口返回Rows是否存在
func BurgeonRowsExists(js_string string) (Exist bool) {
	return gjson.Get(js_string, "0.rows.0").Exists()
}

//获取伯俊接口返回的数据行
func GetBurgeonRowsArray(js_string string) (rows []gjson.Result) {
	return gjson.Get(js_string, "0.rows").Array()
}

//变更数据,新增数据Burgeo返回数据处理
func BurgeonApiResult(jsResponse string) error {
	code, msg := GetBurgeonCodeAndMessage(jsResponse)
	if code == 0 {
		return nil
	} else {
		return errors.New(msg)
	}
}
