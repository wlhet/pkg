package api

import (
	"gitee.com/wlhet/pkg/burgeon/util"

	"gitee.com/wlhet/pkg/burgeon"

	"gitee.com/wlhet/pkg/gjson"
)

//获取Vip当前积分
func (api *Api) GetVipIntegralByCardNO(card string) (int64, error) {
	pst := api.burgeon.NewQuery()
	pst.QuerySetTable("C_VIP")
	pst.QuerySetCondition("CARDNO", card)
	pst.QuerySetResult("INTEGRAL")
	s, err := api.burgeon.Post(pst)
	if err != nil {
		return -1, err
	} else {
		return gjson.Get(s, "0.rows.0.0").Int(), nil
	}
}

//获取Vip等级
func (api *Api) GetVipLevelByCardNO(card string) (int64, error) {
	pst := api.burgeon.NewQuery()
	pst.QuerySetTable("C_VIP")
	pst.QuerySetCondition("CARDNO", card)
	pst.QuerySetResult("VIP_LEVEL")
	s, err := api.burgeon.Post(pst)
	if err != nil {
		return ApiErrCode, err
	} else {
		return gjson.Get(s, "0.rows.0.0").Int(), nil
	}
}

//获取VIP等级名称
func (api *Api) GetVipLevelNameByCard(card string) (string, error) {
	pst := api.burgeon.NewQuery()
	pst.QuerySetTable("C_VIP")
	pst.QuerySetCondition("CARDNO", card)
	pst.QuerySetResult("C_VIPTYPE_ID;NAME")
	s, err := api.burgeon.Post(pst)
	if err != nil {
		return EmptyString, err
	} else {
		name := gjson.Get(s, "0.rows.0.0").String()
		return name, nil
	}
}

func (api *Api) GetVipLevelUpMRetail(card string) (string, error) {
	pst := api.burgeon.NewQuery()
	pst.QuerySetTable("C_VIP")
	pst.QuerySetCondition("CARDNO", card)
	pst.QuerySetResult("M_RETAIL_ID;NAME")
	s, err := api.burgeon.Post(pst)
	if err != nil {
		return EmptyString, err
	} else {
		name := gjson.Get(s, "0.rows.0.0").String()
		return name, nil
	}
}

//获取Vip消费记录/返回消费金额,积分,日期[[2165,44,20210304]]
func (api *Api) GetVipRecordsOfConsumptionByCard(card string) ([][]int64, error) {
	pst := api.burgeon.NewQuery()
	pst.QuerySetTable("FA_VIPINTEGRAL_FTP")
	pst.QuerySetCondition("C_VIP_ID;CARDNO", card)
	pst.QuerySetResult("AMT_ACTUAL", "INTEGRAL", "CHANGDATE")
	s, err := api.burgeon.Post(pst)
	result := make([][]int64, 0)
	if err != nil {
		return result, err
	} else {
		array := util.GetBurgeonRowsArray(s)
		for _, v := range array {
			vv := v.Array()
			result = append(result, []int64{vv[0].Int(), vv[1].Int(), vv[2].Int()})
		}
		return result, nil
	}
}

//新增VIP
func (api *Api) CreateVip(cardinfo *burgeon.VipCardConfig, card, mobil, name, birthday, sex, remark string) error {
	switch sex {
	case "M":
	case "W":
	case "男":
		sex = "M"
	case "女":
		sex = "W"
	default:
		sex = "N"
	}
	pst := api.burgeon.NewObjectCreate()
	pst.ObjectCreateSetTable("C_V_ADDVIP")
	pst.ObjectCreateSetColumn("C_VIPTYPE_ID__NAME", cardinfo.VipType)
	pst.ObjectCreateSetColumn("CARDNO", card)
	pst.ObjectCreateSetColumn("C_CUSTOMER_ID__NAME", cardinfo.Customer)
	pst.ObjectCreateSetColumn("C_STORE_ID__NAME", cardinfo.Store)
	pst.ObjectCreateSetColumn("MOBIL", mobil)
	pst.ObjectCreateSetColumn("SEX", sex)
	pst.ObjectCreateSetColumn("VIPNAME", name)
	pst.ObjectCreateSetColumn("BIRTHDAY", birthday)
	pst.ObjectCreateSetColumn("C_INTEGRALAREA_ID__NAME", cardinfo.IntegralArea)
	pst.ObjectCreateSetColumn("OPENID", " ")
	pst.ObjectCreateSetColumn("DESCRIPTION", remark)
	jsResponse, err := api.burgeon.Post(pst)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}

//[{"message":"修改的记录数: 1","id":"3","code":0}]
//修改VIP生日
func (api *Api) ChangeVipBirthday(card string, birthday string) error {
	pts := api.burgeon.NewObjectModify()
	pts.ObjectModifySetTable("c_vip")
	pts.ObjectModifySetColumn("ak", card)
	pts.ObjectModifySetColumn("birthday", birthday)
	pts.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}

func (api *Api) ChangeVipName(card string, name string) error {
	pts := api.burgeon.NewObjectModify()
	pts.ObjectModifySetTable("c_vip")
	pts.ObjectModifySetColumn("ak", card)
	pts.ObjectModifySetColumn("vipname", name)
	pts.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}

func (api *Api) ChangeVipSex(card string, sex string) error {
	pts := api.burgeon.NewObjectModify()
	pts.ObjectModifySetTable("c_vip")
	pts.ObjectModifySetColumn("ak", card)
	pts.ObjectModifySetColumn("sex", sex)
	pts.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}

func (api *Api) ChangeVipWechatIfSub(card string, sub string) error {
	pts := api.burgeon.NewObjectModify()
	pts.ObjectModifySetTable("c_vip")
	pts.ObjectModifySetColumn("ak", card)
	pts.ObjectModifySetColumn("is_wechat", sub)
	pts.ObjectModifySetColumn("partial_update", true) //partial_update*boolean缺省值:true，表示仅修改传入的<column-name>对应的列
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}

//新增VIP积分调整单
func (api *Api) AddVipIntegral(card, billdate, remark string, integral int64) error {
	pts := api.burgeon.NewProcessOrder()
	pts.ProcessOrderMasterObjSetTable("C_VIPINTEGRALADJ")
	pts.ProcessOrderIfSubmit(true)
	pts.ProcessOrderMasterObjSetColumn("BILLDATE", billdate)
	pts.ProcessOrderMasterObjSetColumn("ADJTYPE", 1)
	pts.ProcessOrderDetailObjsSetTables("C_VIPINTEGRALADJITEM")
	pts.ProcessOrderDetailObjsAddrefobjs("C_VIPINTEGRALADJITEM",
		map[string]interface{}{
			"C_VIP_ID__CARDNO": card,
			"INTEGRALADJ":      integral,
			"DESCRIPTION":      remark,
		})
	jsResponse, err := api.burgeon.Post(pts)
	if err != nil {
		return err
	}
	return util.BurgeonApiResult(jsResponse)
}
