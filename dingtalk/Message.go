package dingtalk

import (
	"errors"
	"fmt"
	"time"
)

//{"msgtype":"text","text":{"content":"请提交日报。"}
func (dt *DingTalk) SendMsgToUser(userid, msg string) error {
	token := dt.GetToken()
	apiUrl := fmt.Sprintf(sendMsgToUserUrl, token)
	PostData := map[string]interface{}{
		"msg": map[string]interface{}{
			"msgtype": "text",
			"text": map[string]string{
				"content": msg,
			},
		},
		"agent_id":    dt.AgentId,
		"userid_list": userid,
	}
	var backmsg BaseResponse
	err := PostJsonPtr(apiUrl, PostData, &backmsg)
	if err == nil {
		if backmsg.ErrCode == 0 {
			return nil
		} else if backmsg.ErrCode == -1 { //发送消息系统异常,最多尝试三次
			for i := 0; i < 3; i++ {
				PostJsonPtr(apiUrl, PostData, &backmsg)
				if backmsg.ErrCode == 0 {
					return nil
				} else {
					time.Sleep(time.Second * 5)
				}
			}
			return errors.New("尝试重发三次失败,信息:" + backmsg.ErrMsg)
		} else {
			return errors.New(backmsg.ErrMsg)
		}
	} else {
		return errors.New(backmsg.ErrMsg)
	}
}
